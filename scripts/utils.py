import glob
import json
import os

def read_json_file(filename):
    with open(filename, "r") as fobj:
        data = json.load(fobj)
    return data

def write_json_file(filename, data, pretty=False):
    with open(filename, "w") as fobj:
        if pretty:
            fobj.write(json.dumps(data, sort_keys=True, indent=4))
        else:
            fobj.write(json.dumps(data))

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def json_response(data):
    print 'Content-Type: application/json\n\n'
    print json.dumps(data)

def get_task_ids(path):
    fls = glob.glob(os.path.join(path, "task_*"))
    ids = [os.path.basename(x).split("task_")[1] for x in fls]
    return ids
