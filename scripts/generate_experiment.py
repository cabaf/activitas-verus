import glob
import os
import random

from argparse import ArgumentParser

from utils import chunks
from utils import get_task_ids
from utils import read_json_file
from utils import write_json_file

def generate_properties_file(p, properties_filename):
    txt = """
            ######################################
            ## External HIT Properties
            ######################################
            title:%s
            description:%s
            keywords:%s
            reward:%s
            assignments:%s
            annotation:%s
            assignmentduration:%s
            hitlifetime:%s
            autoapprovaldelay:%s
          """ % (p["title"], p["description"], p["keywords"], p["rewards"],
                 p["assignments"], p["annotation"], p["assignmentduration"],
                 p["hitlifetime"], p["autoapprovaldelay"])
    if p["qualification"]:
        for idx in len(p["qualification"]):
            txt += """
                     qualification.%d:%s
                     qualification.comparator.%d:%s
                   """ % (idx, p["qualification"][idx]["id"],
                          idx, p["qualification"][idx]["comparator"])
    with open(properties_filename, "w") as fobj:
        fobj.write(txt)

def generate_question_file(question_filename):
    txt = """<?xml version="1.0"?>
<ExternalQuestion xmlns="http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2006-07-14/ExternalQuestion.xsd">
  <ExternalURL>$urls</ExternalURL>
  <FrameHeight>600</FrameHeight>
</ExternalQuestion>
          """
    with open(question_filename, "w") as fobj:
        fobj.write(txt)

def generate_tasks(videos, properties):
    activity_classes = videos.keys()
    task_id = 1
    for cls in activity_classes:
        unknown_chunks = list(chunks(videos[cls]["unknown"],
                                     properties["nr_unkwnon"]))
        for unknown in unknown_chunks:
            random.shuffle(videos[cls]["tp"])
            random.shuffle(videos[cls]["fp"])
            tp = videos[cls]["tp"][:properties["nr_tp"]]
            fp = videos[cls]["fp"][:properties["nr_fp"]]
            task = {"unknown": unknown, "taskid": "%09d" % task_id,
                    "tp": tp, "fp": fp, "activity": cls}
            filename = os.path.join(properties["path"],
                                    "task_%09d" % task_id)
            write_json_file(filename, task, True)
            task_id += 1
    print "%d task files were created." % (task_id-1)

def generate_input_file(task_path, task_url, experiment_id,
                        task_mode, input_filename):
    ids = get_task_ids(task_path)
    txt = "urls\n"
    url = task_url + "/?taskId=%s&expId=%s&mode=" + task_mode
    for x in ids: txt += url % (x, experiment_id) + "\n"
    with open(input_filename, "w") as fobj:
        fobj.write(txt)

def config_experiment(config):
    if not os.path.exists(config["paths"]["mturk"]):
        os.makedirs(config["paths"]["mturk"])
    if not os.path.exists(config["paths"]["tasks"]):
        os.makedirs(config["paths"]["tasks"])
    if not os.path.exists(config["paths"]["results"]):
        os.makedirs(config["paths"]["results"])
        os.chmod(config["paths"]["results"], 0o777)

def generate_experiment(input_file, config_file):
    videos = read_json_file(input_file)
    config = read_json_file(config_file)
    config_experiment(config["setup"])
    generate_tasks(videos, config["task"])
    generate_input_file(config["task"]["path"], config["setup"]["url"],
                        config["setup"]["experiment_id"],
                        config["setup"]["mode"],
                        config["mturk"]["input_filename"])
    generate_properties_file(config["mturk"]["properties"],
                             config["mturk"]["properties_filename"])
    generate_question_file(config["mturk"]["question_filename"])


if __name__ == "__main__":
    parser = ArgumentParser(description="This script generates an experiment \
                                         to verify the presence of human \
                                         activities in videos.")
    parser.add_argument("input_file", help="JSON file containing the input \
                                            videos to verify.")
    parser.add_argument("config_file", help="File containing the config of \
                                                 the experiment.")
    generate_experiment(**vars(parser.parse_args()))
