#!/usr/bin/python
import cgi
import json
import os
import random

from config import get_config
from utils import read_json_file
from utils import write_json_file
from utils import json_response


def submit_answers(form, config):
    # Validate first.
    path = config["setup"]["paths"]["tasks"]
    if form.getvalue("taskid"):
        filename = os.path.join(path, "task_" + form.getvalue("taskid"))
        data = read_json_file(filename)
    else:
        json_response(None)
        return
    videoids = json.loads(form.getvalue("videoids"))
    answers = json.loads(form.getvalue("answers"))
    seeds = data["tp"][:2] + data["fp"][:2]
    seeds_answers = [1, 1, 0, 0]
    state = True
    for jdx, x in enumerate(seeds):
        if x not in videoids:
            state = False
            break
        else:
            idx = videoids.index(x)
            this_answer = answers[idx]
            if not (this_answer==seeds_answers[jdx]):
                state = False
                break
    # Write results.
    filename = os.path.join(config["setup"]["paths"]["results"],
                            "task_" + form.getvalue("assignmentid"))
    if state:
        unknown = data["unknown"]
        results = {"tp": [], "fp": [],
               "assignmentid": form.getvalue("assignmentid"),
               "workerid": form.getvalue("workerid"),
               "taskid": form.getvalue("taskid"),
               "raw": json.loads(form.getvalue("raw"))}
        for x in unknown:
            idx = videoids.index(x)
            if answers[idx]==1:
                results["tp"].append(x)
            else:
                results["fp"].append(x)        
        write_json_file(filename, results)
    json_response(state)

def get_task_data(form, config):
    path = config["setup"]["paths"]["tasks"]
    if form.getvalue("taskid"):
        filename = os.path.join(path, "task_" + form.getvalue("taskid"))
        data = read_json_file(filename)
        data["unknown"].extend(data["fp"][:2]+data["tp"][:2])
        random.shuffle(data["unknown"])
        data["positive"] = data["tp"][2:]
        data["negative"] = data["fp"][2:]
        del data["fp"], data["tp"]
    else:
        data = None
    json_response(data)

if __name__ == "__main__":
    form = cgi.FieldStorage()
    action_name = form.getvalue("action")
    experiment_id = form.getvalue("expid")
    if not (action_name) and not (experiment_id):
        raise Exception("Missed required key.")
    config = get_config(experiment_id)
    possibles = globals().copy()
    action = possibles.get(action_name)
    if not action:
        raise Exception("Action %s not recognized" % action_name)
    action(form, config)
