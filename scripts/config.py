import json
import os

from argparse import ArgumentParser

PROJECT_PATH=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


"""
   Global setup.
"""
#EXPERIMENT_ID="anet_110_verification"
URL="https://yamdrok.stanford.edu/activitas"
MODE="production"


def get_config(EXPERIMENT_ID):
    MTURK_CONFIG_PATH=os.path.join(PROJECT_PATH,
                                   "experiments/%s/mturk.properties" % EXPERIMENT_ID)
    TASKS_PATH=os.path.join(PROJECT_PATH,
                                   "experiments/%s/tasks.input" % EXPERIMENT_ID)
    RESULTS_PATH=os.path.join(PROJECT_PATH,
                                   "experiments/%s/tasks.output" % EXPERIMENT_ID)
    config = {}
    config["setup"] = {
                        "experiment_id": EXPERIMENT_ID,
                        "url": URL,
                        "mode": MODE,
                        "paths": {
                                    "mturk": MTURK_CONFIG_PATH,
                                    "tasks": TASKS_PATH,
                                    "results": RESULTS_PATH
                                 }
                       }
    """
      Task config.
    """
    config["task"] = {
                      "nr_tp": 4,
                      "nr_fp": 4,
                      "nr_unkwnon": 12,
                      "path": config["setup"]["paths"]["tasks"]
                     }

    """
      Mturk config.
    """
    INPUT_FILENAME=os.path.join(config["setup"]["paths"]["mturk"],
                               EXPERIMENT_ID + ".input")
    QUESTION_FILENAME=os.path.join(config["setup"]["paths"]["mturk"],
                               EXPERIMENT_ID + ".question")
    PROPERTIES_FILENAME=os.path.join(config["setup"]["paths"]["mturk"],
                               EXPERIMENT_ID + ".properties")
    config["mturk"] = {
                       "input_filename": INPUT_FILENAME,
                       "question_filename": QUESTION_FILENAME,
                       "properties_filename": PROPERTIES_FILENAME,
                       "properties": {}
                     }

    """
       READ THIS!!!
    """
    config["mturk"]["properties"] = \
    {
        "title": "Video analysis",
        "description": "Verify and evaluate huaman activities in video sequences",
        "keywords": "video,human,activities,tag,tagging,image,videos,images,visual",
        "rewards": "0.05",
        "assignments": "5",
        "annotation": "${urls}",
        "assignmentduration": "3600",
        "hitlifetime": "1296000",
        "autoapprovaldelay": "1296000",
        "qualification": []
    }
    return config

if __name__=="__main__":
    parser = ArgumentParser(description="Generates config file.")
    parser.add_argument("id", help="Experiment id.")
    parser.add_argument("--filename", help="Output filename.")
    args = parser.parse_args()
    EXPERIMENT_ID = args.id
    config = get_config(EXPERIMENT_ID)
    if args.filename is not None:
        filename = args.filename
    else:
        filename = EXPERIMENT_ID + ".json"
    with open(filename, "w") as fobj:
        fobj.write(json.dumps(config, indent=4))
