function parse_answer(answer) {
  if ("Yes"==answer) {
    return true;
  }
  if ("No"==answer) {
    return false;
  }
}

function print_video(videoid) {
  var base_url = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" width="604" height="483" src="https://www.youtube.com/embed/%s?&modestbranding=1&showinfo=0&fs=0&autoplay=1&rel=0" frameborder="0"></iframe></div>'
  var url = sprintf(base_url, videoid);
  return url;
}

function send_to_amt(assignment_id, worker_id, mode) {
  if (mode=="sandbox") {
    var amt_url = "https://workersandbox.mturk.com/mturk/externalSubmit";
  }
  else {
    var amt_url = "https://www.mturk.com/mturk/externalSubmit";
  }
  var html_form = sprintf("<form id='amt_form' name='result' method='post' action='%s'>" +
                          "<input type='hidden' name='assignmentId' value='%s'>" +
                          "<input type='hidden' name='workerId' value='%s'>" +
                          "</form>", amt_url, assignment_id, worker_id);
  $("body").append(html_form);
  $("#amt_form").submit();
}


function get_parameters() {
  var retval = new Object();
  if (window.location.href.indexOf("?") == -1) {
    retval.mode = null;
    retval.experimentid = null;
    retval.workerid = null;
    retval.taskid = null;
    retval.assignmentid = null;
    retval.hitid = null;
    return retval;
  }
  var  params = window.location.href.split("?")[1].split("&");
  for (var i in params) {
    var sp = params[i].split("=");
    if (sp.length <= 1) {
      continue;
    }
    var result = sp[1].split("#")[0];
    if (sp[0] == "mode")
    {
      retval.mode = result;
    }
    else if (sp[0] == "expId")
    {
        retval.experimentid = result;
    }
    else if (sp[0] == "workerId")
    {
        retval.workerid = result;
    }
    else if (sp[0] == "taskId")
    {
        retval.taskid = result;
    }
    else if (sp[0] == "assignmentId")
    {
        retval.assignmentid = result;
    }
    else if (sp[0] == "hitId")
    {
        retval.hitid = result;
    }
    else
    {
        retval[sp[0]] = result;
    }
  }
  return retval;
}
