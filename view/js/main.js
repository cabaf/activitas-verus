var server_url = "https://yamdrok.stanford.edu/activitas_server/server.py"
var params;
var task_data;
var answers = new Object();
answers.related = new Array();
answers.animation = new Array();
answers.performing = new Array();
jQuery(function($) {
  params = get_parameters();
  var html_str = '<iframe height="480px" width="100%" src="guidelines/?taskId=' + params.taskid + '&expId=' + params.experimentid + '"></iframe>';
  bootbox.dialog({
    size: 'large',
    message: html_str,
  });


  $("#btn-instructions").click(function(){
    var html_str = '<iframe height="480px" width="100%" src="guidelines/?taskId=' + params.taskid + '&expId=' + params.experimentid + '"></iframe>';
    bootbox.dialog({
      size: 'large',
      message: html_str,
    });
  });

  if (params.workerid && params.assignmentid && params.hitid) {
    if (!params.taskid) {
      $("body").html("Unknown task.");
    }
    else {
      //$("#workerid").text(params.workerid);
      $('#submit-button').removeClass("disabled");
      $('#submit-button').text("Submit your answers!");
      $('#submit-button').click(function(){
        if (validate_answers(answers)) {
          //console.log("Finished.");
          consolidate_answers(answers)
          submit_answers(task_data["unknown"], answers.consolidated);
        }
        else {
          bootbox.dialog({
            size: 'large',
            message: "<h3>Please check your answers, our system detects you violate the guidelines.</h3>",
          });
        }
      });
    }
  }
  else {
    $("body").html("<div class=\"row\" style=\"background-color: #DFD;\"><h1>Accept HIT before start to work.</h1></div>");
  }

  $.ajax({
    url: server_url,
    type: "POST",
    dataType: "json",
    data: {action: "get_task_data", taskid: params.taskid,
           expid: params.experimentid},
    success: function(data) {
      task_data = data;
      boot_ui(task_data);
    }
  });
});

function print_progress(answers) {
  var nr_videos = task_data.unknown.length;
  var cnt = 0;
  for (var i=0; i<nr_videos; i++) {
    if ((typeof answers.related[i] !== 'undefined') &&
       (typeof answers.animation[i] !== 'undefined') &&
       (typeof answers.performing[i] !== 'undefined')) {
      cnt++;
    }
  }
  var html_str = sprintf("<b>%d videos completed of %d videos in total.</b>", cnt, nr_videos);
  $("#progress").html(html_str);
}

function consolidate_answers(answers) {
  answers.consolidated = new Array();
  var nr_videos = task_data.unknown.length;
  for (var i=0; i<nr_videos; i++) {
    answers.consolidated[i] = answers.performing[i];
    if (answers.animation[i]) {
      answers.consolidated[i] = false;
    }
  }
}

function validate_answers(answers) {
  var nr_videos = task_data.unknown.length;
  state = true;
  if ((answers.related.length != nr_videos) ||
      (answers.animation.length != nr_videos) ||
      (answers.performing.length != nr_videos)) {
    state = false;
    return state
  }

  for (var i=0; i<nr_videos; i++) {
    
    if ((typeof answers.related[i] === 'undefined') || 
       (typeof answers.animation[i] === 'undefined') || 
       (typeof answers.performing[i] === 'undefined')) {
      state = false;
      break;
    }
    if ((answers.related[i]==false) && (answers.performing[i]==true)) {
      state = false;
      break;
    }
  }
  return state;
}

var current_page = 1;
function boot_ui(task_data) {
  var nr_videos = task_data.unknown.length;
  // Setup pagination.
  $("#page-selection").bootpag({
    total: nr_videos,
    page: current_page,
    prev: "Previous",
    next: "Next",
    maxVisible: 8
  }).on("page", function(event, page){
    current_page = page;
    next_video(current_page);
    next_form(current_page);
    print_progress(answers);
  });
  next_video(current_page);
  next_form(current_page);
  print_progress(answers);
}

function next_video(page) {
  var videoid = task_data["unknown"][page-1];
  $("#video-player").html(print_video(videoid));
}

function next_form(page) {
  console.log(task_data["unknown"][page-1]);
  $.ajax({
    url: "form.html",
    success: function(raw) {
      var html = sprintf(raw, task_data.activity, task_data.activity);
      $("#form-div").html(html);
      // Store answers.
      $("#form-related").on('change', function(event){
        var this_answer = $('#form-related input:radio:checked').val();
        answers.related[current_page-1] = parse_answer(this_answer);
      });
      $("#form-animation").on('change', function(event){
        var this_answer = $('#form-animation input:radio:checked').val();
        answers.animation[current_page-1] = parse_answer(this_answer);
      });
      $("#form-performing").on('change', function(event){
        var this_answer = $('#form-performing input:radio:checked').val();
        answers.performing[current_page-1] = parse_answer(this_answer);
      });
    check_if_answered(current_page-1);
    }
  });
}

function check_if_answered(page) {
  if  (typeof answers.related[page] !== 'undefined') {
    if (answers.related[page]) {
      var value = "Yes";
    }
    else {
      var value = "No";
    }
    $("input[name=form-related][value=" + value + "]").parent('.btn').addClass('active');
  }
  if  (typeof answers.animation[page] !== 'undefined') {
    if (answers.animation[page]) {
      var value = "Yes";
    }
    else {
      var value = "No";
    }
    $("input[name=form-animation][value=" + value + "]").parent('.btn').addClass('active');
  }
  if  (typeof answers.performing[page] !== 'undefined') {
    if (answers.performing[page]) {
      var value = "Yes";
    }
    else {
      var value = "No";
    }
    $("input[name=form-performing][value=" + value + "]").parent('.btn').addClass('active');
  }
}


function submit_answers(videoids, this_answers) {
  $.ajax({
    url: server_url,
    type: "POST",
    dataType: "json",
    data: {action: "submit_answers", taskid: params.taskid,
           expid: params.experimentid, videoids: JSON.stringify(videoids),
           answers: JSON.stringify(this_answers),
           assignmentid: params.assignmentid,
           workerid: params.workerid,
           raw: JSON.stringify(answers)},
    success: function(result) {
      if (result) {
        send_to_amt(params.assignmentid, params.workerid, params.mode);
      }
      else {
        bootbox.dialog({
          size: 'large',
          message: "<h3>Please check your answers, our system detects you violate the guidelines.</h3>",
        });
      }
    }
  });
}
