var server_url = "https://yamdrok.stanford.edu/activitas_server/server.py"
jQuery(function($) {
  params = get_parameters();
  $.ajax({
    url: server_url,
    type: "POST",
    dataType: "json",
    data: {action: "get_task_data", taskid: params.taskid,
           expid: params.experimentid},
    success: function(data) {
      $(".activity").text(data.activity);
      $("#tp-1").html(print_video(data.positive[0]));
      $("#tp-2").html(print_video(data.positive[1]));
      $("#fp-1").html(print_video(data.negative[0]));
      $("#fp-2").html(print_video(data.negative[1]));
    }
  });
});

function print_video(videoid) {
  var base_url = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" width="604" height="483" src="https://www.youtube.com/embed/%s?&modestbranding=1&showinfo=0&fs=0&autoplay=1&rel=0" frameborder="0"></iframe></div>'
  var url = sprintf(base_url, videoid);
  return url;
}

function get_parameters() {
  var retval = new Object();
  if (window.location.href.indexOf("?") == -1) {
    retval.mode = null;
    retval.experimentid = null;
    retval.workerid = null;
    retval.taskid = null;
    retval.assignmentid = null;
    retval.hitid = null;
    return retval;
  }
  var  params = window.location.href.split("?")[1].split("&");
  for (var i in params) {
    var sp = params[i].split("=");
    if (sp.length <= 1) {
      continue;
    }
    var result = sp[1].split("#")[0];
    if (sp[0] == "mode")
    {
      retval.mode = result;
    }
    else if (sp[0] == "expId")
    {
        retval.experimentid = result;
    }
    else if (sp[0] == "workerId")
    {
        retval.workerid = result;
    }
    else if (sp[0] == "taskId")
    {
        retval.taskid = result;
    }
    else if (sp[0] == "assignmentId")
    {
        retval.assignmentid = result;
    }
    else if (sp[0] == "hitId")
    {
        retval.hitid = result;
    }
    else
    {
        retval[sp[0]] = result;
    }
  }
  return retval;
}
